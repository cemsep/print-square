﻿using System;

namespace Print_square
{
    class Program
    {
        static void Main(string[] args)
        {

            int height = 0;
            int width = 0;

            void userInput(ref int height, ref int width) {
                Console.WriteLine("Enter the height of the square/rectangle (positive integer only!):");

                while (!int.TryParse(Console.ReadLine(), out height) || height <= 0)
                {
                    Console.WriteLine("Invalid input! Please enter a positive integer...");
                }

                Console.WriteLine("Enter the width of the square/rectangle (positive integer only!):");

                while (!int.TryParse(Console.ReadLine(), out width) || width <= 0)
                {
                    Console.WriteLine("Invalid input! Please enter a positive integer...");
                }
            }

            void drawSquare(int height, int width) {
                for (int i = 0; i < height; i++)
                {
                    for (int j = 0; j < width; j++)
                    {
                        Console.Write('#');
                    }
                    Console.Write('\n');
                }
            }

            userInput(ref height, ref width);
            drawSquare(height, width);

        }
    }
}
